/**
 * @file js/collapse.js
 * This file controls the public facing javascript for the module.
 */

$ = jQuery;

/**
 * When the page has loaded, run the init function.
 */
$(document).ready(function() {
  $(".wysiwyg_collapsible").each(function() {
    wcs_initCollapsibleSection($(this));
  });
});

/**
 * Hide source areas, set up handler for clicking the target areas.
 */
function wcs_initCollapsibleSection(target) {
  var targetArea = target.find(".wysiwyg_collapsible_target");
  var sourceArea = target.find(".wysiwyg_collapsible_source");
  
  sourceArea.hide();
  
  targetArea.click(function() {
    if (target.hasClass("active")) {
      target.removeClass("active");
      sourceArea.slideUp();
    } else {
      target.addClass("active");
      sourceArea.slideDown();
    }
  });
}