/**
 * @file collapse/plugin.js
 * This file controls the CKEditor plugin to control the admin side of the module.
 */

$ = jQuery;

CKEDITOR.plugins.add( 'collapse', {
  init: function( editor ){
    editor.addCommand( 'wc_insert', {
      exec : function( editor ) {
        // get current insertion position
        var position = editor.getSelection().getStartElement();
        
        // check if nested.
        if (!wcs_isNested(position)) {
          editor.insertHtml('<div class="wysiwyg_collapsible"><div id="target" class="wysiwyg_collapsible_target"><div>Clickable Title</div></div><div id="source" class="wysiwyg_collapsible_source"><div class="wysiwyg_source_element">Hideable Content</div></div></div>' );
        } else {
          alert("You can not place a collapsible section within another collapsible section.")
        }
      }
    });
    editor.ui.addButton( 'collapse_button', {
      label: 'Collapsible Sections',
      command: 'wc_insert',
      icon: this.path + 'images/collapse.png'
    });
    editor.config.contentsCss = this.path + 'css/collapse.css';
  }
});

/**
 * Validate that insertion position is inside of another collapsible section.
 */
function wcs_isNested(position) {
  // see if the parent's parent has a class of .wysiwyg_collapsible.
  // if it does, return true. if not, return false.
  
  try {
    var div_parent = position.getParent().getParent();
    return div_parent.hasClass("wysiwyg_collapsible");
  } catch(err) {
    
    // if a parent doesn't exist, return false.
    return false;
  }
}